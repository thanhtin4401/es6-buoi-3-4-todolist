export let toDoListController = {
    getInputTask: () => {
        let content = document.getElementById('newTask').value;
        let inputNewTask = {
            content: content
        }
        return inputNewTask;
    },
    clearInputTask: () => {
        document.getElementById('newTask').value = "";
    }
}