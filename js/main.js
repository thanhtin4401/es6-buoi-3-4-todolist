
import { toDoListFunction, toDoListFunctionChecked } from './service/toDoListActive.js'


import { toDoListController } from '../controller/toDoListController.js'

let renderToDoList = (list) => {
    let HTML = "";
    for (var i = 0; i < list.length; i++) {
        let Task = list[i];
        let contentLi = `
        <li>
         <span>${Task.content}</span>
            <div class="buttons">
                <button class="remove" onclick="deleteTask(${Task.id})">
                     <i id="remove" class="fa-solid fa-trash-can"></i>
                </button>
                 <button class="complete"onclick="complete(${Task.id})">
                     <i  class="fa-regular fa-circle-check"></i>
                 </button>
           </div>
        </li>`
        HTML += contentLi;
    }
    document.getElementById('todo').innerHTML = HTML;
}



let renderToDoListChecked = (list) => {
    let HTML = "";
    for (var i = 0; i < list.length; i++) {
        let Task = list[i];
        let contentLi = `
        <li>
        <span>${Task.content}</span>
        <div class="buttons">
            <button>
                <i id="remove" onclick="deleteTaskChecked(${Task.id})" class="fa-solid fa-trash-can remove"></i>
            </button>
            <button class="complete"onclick="completed(${Task.id})">
                <i class="fas fa-solid fa-circle-check"></i>
            </button>
        </div>
        </li>`
        HTML += contentLi;
    }
    document.getElementById('completed').innerHTML = HTML;
}

// render task checked 
let renderTaskListChecked = () => {
    toDoListFunctionChecked.getToDoListChecked()
        .then((res) => {
            renderToDoListChecked(res.data);
        })
        .catch((err) => {
            console.log(err);
        })
}
renderTaskListChecked();

//render task list 
let renderTaskList = () => {
    toDoListFunction.getToDoList()
        .then((res) => {
            renderToDoList(res.data);
        })
        .catch((err) => {
            console.log(err);
        })

}
renderTaskList();

// add new task list
let addNewTask = () => {
    let Task = toDoListController.getInputTask();
    toDoListFunction.addToDoList(Task)
        .then((res) => {
            renderTaskList();
            toDoListController.clearInputTask();
        })
        .catch((err) => {
            console.log(err);
        })
}

document.getElementById("addItem").onclick = addNewTask;

// check task to do list
let complete = (id) => {
    toDoListFunction.getContent(id)
        .then((res) => {
            toDoListFunctionChecked.addToDoListChecked(res.data)
                .then((res) => {
                    renderTaskListChecked();
                    toDoListFunction.deleteTask(id)
                        .then((res) => {
                            renderTaskList();
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                })
                .catch((err) => {
                    console.log(err);
                })
        })
        .catch((err) => {
            console.log(err);
        })
}

// remove check task to do list
let completed = (id) => {
    toDoListFunctionChecked.getContentChecked(id)
        .then((res) => {
            toDoListFunction.addToDoList(res.data)
                .then((res) => {
                    renderTaskList();
                    toDoListFunctionChecked.deleteTaskChecked(id)
                        .then((res) => {
                            renderTaskListChecked();
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                })
                .catch((err) => {
                    console.log(err);
                })
        })
        .catch((err) => {
            console.log(err);
        })
}


// task is checked
document.getElementById("")



// delete Task
let deleteTask = (idTask) => {
    toDoListFunction.deleteTask(idTask)
        .then((res) => {


            renderTaskList();
        })
        .catch((err) => {
            console.log(err);
        })
}

// delete Task checked
let deleteTaskChecked = (idTask) => {
    toDoListFunctionChecked.deleteTaskChecked(idTask)
        .then((res) => {
            renderTaskListChecked();
        })
        .catch((err) => {
            console.log(err);
        })
}


// sort ascending
let ascending = () => {
    let content = [];
    toDoListFunction.getToDoList()
        .then((res) => {
            content = res.data;
            let sort = content.sort((a, b) => {
                return a.content.localeCompare(b.content);
            })
            renderToDoList(sort);
        })
        .catch((err) => {
            console.log(err);
        });
    toDoListFunctionChecked.getToDoListChecked()
        .then((res) => {
            content = res.data;
            let sort = content.sort((a, b) => {
                return a.content.localeCompare(b.content);
            })
            renderToDoListChecked(sort);
            // console.log(content);
        })
        .catch((err) => {
            console.log(err);
        })

}

// sort ascendingTime
let ascendingTime = () => {
    toDoListFunction.getToDoList()
        .then((res) => {
            let dates = res.data;

            let sort = dates.sort((a, b) => {
                let d1 = new Date(a.date);
                let d2 = new Date(b.date);
                let t1 = d1.getTime();
                let t2 = d2.getTime();
                return t1 - t2;
            })

            renderToDoList(sort);
        })
        .catch((err) => {
            console.log(err);
        });
    toDoListFunctionChecked.getToDoListChecked()
        .then((res) => {
            content = res.data;
            let sort = content.sort((a, b) => {
                return a.content.localeCompare(b.content);
            })
            renderToDoListChecked(sort);
        })
        .catch((err) => {
            console.log(err);
        })

}

document.getElementById("all").onclick = ascendingTime;


// sort decrease
let decrease = () => {
    let content = [];
    toDoListFunction.getToDoList()
        .then((res) => {
            content = res.data;
            let sort = content.sort((a, b) => {
                return b.content.localeCompare(a.content);
            })
            renderToDoList(sort);
        })
        .catch((err) => {
            console.log(err);
        });
    toDoListFunctionChecked.getToDoListChecked()
        .then((res) => {
            content = res.data;
            let sort = content.sort((a, b) => {
                return a.content.localeCompare(b.content);
            })
            renderToDoListChecked(sort);
        })
        .catch((err) => {
            console.log(err);
        })
}



document.getElementById("two").onclick = ascending;
document.getElementById("three").onclick = decrease;
window.ascendingTime = ascendingTime;
window.ascending = ascending;
window.decrease = decrease;
window.renderToDoListChecked = renderToDoListChecked;
window.renderTaskListChecked = renderTaskListChecked;
window.completed = completed;
window.complete = complete;
window.deleteTaskChecked = deleteTaskChecked;
window.deleteTask = deleteTask;
window.addNewTask = addNewTask;
window.renderTaskList = renderTaskList;
window.renderToDoList = renderToDoList;


// filter task list
document.getElementById('one').onclick = function () {
    document.getElementById('todo').classList.toggle('none');
}