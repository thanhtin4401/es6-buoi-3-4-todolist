const BASE_URL = "https://62bea99ebe8ba3a10d58a27a.mockapi.io/toDoList/toDoList"
const BASE_URL_CHECKED = "https://62bea99ebe8ba3a10d58a27a.mockapi.io/toDoList/toDoListChecked"
export let toDoListFunction = {
    addToDoList: (content) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: content
        })

    },
    getToDoList: () => {
        return axios({
            url: BASE_URL,
            method: "GET"
        })

    },
    deleteTask: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE"
        })
    },
    getContent: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "GET"
        })
    }

}

export let toDoListFunctionChecked = {
    addToDoListChecked: (content) => {
        return axios({
            url: BASE_URL_CHECKED,
            method: "POST",
            data: content
        })

    },
    getToDoListChecked: () => {
        return axios({
            url: BASE_URL_CHECKED,
            method: "GET"
        })

    },
    deleteTaskChecked: (id) => {
        return axios({
            url: `${BASE_URL_CHECKED}/${id}`,
            method: "DELETE"
        })
    },
    getContentChecked: (id) => {
        return axios({
            url: `${BASE_URL_CHECKED}/${id}`,
            method: "GET"
        })
    }

}